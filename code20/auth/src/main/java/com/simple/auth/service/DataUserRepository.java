package com.simple.auth.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.simple.auth.entity.HidePassword;

public interface DataUserRepository extends JpaRepository<HidePassword, Integer> {
	@Query (value="SELECT * FROM users", nativeQuery = true)
	List<HidePassword> users();
	
	@Query (value="SELECT * FROM users WHERE id=?1", nativeQuery = true)
	List<HidePassword> userById(int id);
	
	@Query (value="SELECT * FROM users WHERE username=?1", nativeQuery = true)
	List<HidePassword> userByUsername(String username);
}

