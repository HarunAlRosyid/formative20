package com.simple.auth.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.simple.auth.service.DataUserRepository;
import com.simple.auth.service.UserRepository;
import com.simple.auth.entity.HidePassword;
import com.simple.auth.entity.Users;
import com.simple.auth.handler.ResponseHandlers;

@RestController
@RequestMapping("/api")
public class UserController {
	List<Users> dataUser = new ArrayList<Users>(); 
	List<HidePassword> dataShow = new ArrayList<HidePassword>(); 
	
	@Autowired
	UserRepository userRepository;
	@Autowired
	DataUserRepository data;

	@GetMapping("/users")
	public ResponseEntity<ResponseHandlers> getAll() {
		dataShow = data.users();
		if(dataShow.size()==0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).
				body(new ResponseHandlers(HttpStatus.NOT_FOUND.value(),"Users Not Found",dataEmpty()));
		} else {
			return ResponseEntity.status(HttpStatus.OK).
				body(new ResponseHandlers(HttpStatus.OK.value(),"Found List of Users",dataShow));
		}
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<ResponseHandlers> getUserById(@PathVariable int id) {
		dataShow = data.userById(id);
		if(dataShow.size()==0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).
				body(new ResponseHandlers(HttpStatus.NOT_FOUND.value(),"User Not Found",dataEmpty()));
		} else {
			return ResponseEntity.status(HttpStatus.OK).
				body(new ResponseHandlers(HttpStatus.OK.value(),"User Found", dataShow));
		}
	}
	
	@PostMapping("/user")
	public ResponseEntity<ResponseHandlers> createUser(@RequestBody Users payload) {
		if(userExist(payload.getUsername())==true) {
			dataShow = data.userByUsername(payload.getUsername());
			return ResponseEntity.status(HttpStatus.OK).
				body(new ResponseHandlers(HttpStatus.OK.value(),
					"Username already registered",dataShow));
		} else {
			userRepository.insertUser(payload.getUsername(), hash(payload.getPassword()));
			dataShow = data.userByUsername(payload.getUsername());
			return ResponseEntity.status(HttpStatus.CREATED).
				body(new ResponseHandlers(HttpStatus.CREATED.value(),
					"User registered successfully", dataShow));
		}
		
	}
	
	public boolean userExist(String username) {
		List<Users> user = userRepository.userByUsername(username);
		if(user.size()==1) {
			return true;
		} else {
			return false;
		}
	}
	
	@PostMapping("/auth/login")
	public ResponseEntity<ResponseHandlers> auth(@RequestBody Users payload){
		if(userExist(payload.getUsername())==true) {
			dataUser = userRepository.userByUsername(payload.getUsername());
			boolean chekPassword = cekHash(payload.getUsername(),payload.getPassword());
			if(chekPassword==true) {
				userRepository.authLogin(dataUser.get(0).getUsername(), dataUser.get(0).getPassword());
				dataShow = data.userByUsername(payload.getUsername());
				return ResponseEntity.status(HttpStatus.OK).
					body(new ResponseHandlers(HttpStatus.OK.value(),"Authentication successfully ",dataShow));
			} else {
				return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).
					body(new ResponseHandlers(HttpStatus.NOT_ACCEPTABLE.value(),
						"Authentication fail. Please check your password", dataEmpty()));
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).
				body(new ResponseHandlers(HttpStatus.NOT_ACCEPTABLE.value(),
					"Authentication fail. Please check your username and password", dataEmpty()));
		}
		
	}
	
	public String hash(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}
	public boolean cekHash(String username,String password) {
		dataUser = userRepository.userByUsername(username);
		return BCrypt.checkpw(password, dataUser.get(0).getPassword().toString());
	}
	public List<HidePassword> dataEmpty(){
		return data.userByUsername("");
	}
}
