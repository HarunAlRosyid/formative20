package com.simple.auth.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.simple.auth.entity.Users;

public interface UserRepository extends JpaRepository<Users, Integer> {
	@Query (value="SELECT * FROM users", nativeQuery = true)
	List<Users> users();
	
	@Query (value="SELECT * FROM users WHERE id=?1", nativeQuery = true)
	List<Users> userById(int id);
	
	@Query (value="SELECT * FROM users WHERE username=?1", nativeQuery = true)
	List<Users> userByUsername(String username);
	
	@Transactional
	@Modifying
	@Query (value="INSERT INTO users (username,password) VALUES (?1,?2)", nativeQuery = true)
	int insertUser(String username, String password);

	@Query (value="SELECT * FROM users WHERE username=?1 AND password=?2", nativeQuery = true)
	List<Users> authLogin(String username, String password);
}
