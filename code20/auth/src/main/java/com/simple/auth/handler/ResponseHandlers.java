package com.simple.auth.handler;

import java.util.List;

import com.simple.auth.entity.HidePassword;

public class ResponseHandlers  {
	private int status;
	private String message;
	private List<HidePassword> users;
	public ResponseHandlers(int status, String message, List<HidePassword> users) {
		super();
		this.status = status;
		this.message = message;
		this.users = users;
	}

	public ResponseHandlers(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}


	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<HidePassword> getUsers() {
		return users;
	}

	public void setUsers(List<HidePassword> users) {
		this.users = users;
	}	
}
